#pragma once
#include<iostream>
using namespace std;
#define FULLQUEUEEXP 10
#define EMPTYQUEUEEXP 20

template <class Elem>
class queue {

private:
	Elem     *Data;      // wskaznik na dane
	const int MAX_NUM;   // dlugosc kolejki
	int       Beginning, End; // lokacja pierwszego i ostatniego elementu
	int       ElemCount; // licznik ilosci

public:
	queue(int MaxSize = 500);
	~queue();

	void       Enqueue(const Elem &Item);    // dodanie na koniec kolejki
	Elem       Dequeue(void);                // zwraca element z kolejki
	friend ostream & operator<< (ostream &ostr, const queue &q);
	void		Write();					//wypisanie wszystkich elementow w kolejce

};
//konstruktor
template<class Elem>
inline queue<Elem>::queue(int MaxSize) :MAX_NUM(MaxSize)//lista inicjalizacyjna 
{
	Data = new Elem[MAX_NUM + 1];
	Beginning = 0;
	End = 0;
	ElemCount = 0;
}
//dekonstruktor
template<class Elem>
inline queue<Elem>::~queue()
{
	delete[] Data;
}
//dodawanie do kolejki
template<class Elem>
inline void queue<Elem>::Enqueue(const Elem & Item)
{
	// Sprawdzzenie czy kolejka nie jest pelna
	if (ElemCount > MAX_NUM)
	{
		throw FULLQUEUEEXP;
	}

	Data[End++] = Item;
	++ElemCount;

}
//zabieranie z kolejki
template<class Elem>
inline Elem queue<Elem>::Dequeue(void)
{
	// Sprawdzenie czy cos jest na kolejce
	if (ElemCount == 0)
	{
		throw EMPTYQUEUEEXP;
	}

	Elem ReturnValue = Data[Beginning++];
	--ElemCount;


	return ReturnValue;
}

//wypisanie
template<class Elem>
inline void queue<Elem>::Write()
{
	int tmp = 0;
	int tmp2 = Data[0];
	//cout << tmp2;
	tmp = this->Beginning; //ustawienie pomocniczego wskaznika tablicy na poczatek
						   //int a = Data[0];
						   //		int b=this->ElemCount;
	for (int i = 0; i < ElemCount; i++)
	{
		cout << Data[tmp] << endl;
		tmp = tmp + 1;
	}
}






#include"queue.h"



int main()
{
	int menu = 0;

	queue<int> Kolejka;
	try {
		while (menu != 5)
		{

			cout << "kolejka" << endl;
			cout << "1.Dodaj do kolejki" << endl;
			cout << "2.Usun z kolejki" << endl;
			cout << "3.Wyswietl zawartosc" << endl;
			cout << "4.Wyczysc kolejke" << endl;
			cout << "5.Koniec" << endl;
			cout << "Wynik: "; cin >> menu;

			switch (menu)
			{
			case 1:
			{int dodana;
			cout << "Dodana wartosc do kolejki:";
			cin >> dodana;
			Kolejka.Enqueue(dodana);
			}
			break;
			case 2:
				Kolejka.Dequeue(); break;
			case 3:
				cout << "Wyswietlanie:" << endl;
				//cout << Kolejka;
				Kolejka.Write();
				break;
			case 4:
				break;
			case 5: break;
			default:cout << "zly numer" << endl;
				break;
			}
			system("pause");
			system("cls");
		}
	}
	catch (int blad)
	{
		if (blad == FULLQUEUEEXP)
		{
			cout << "Pelna kolejka" << endl;
			system("pause");
		}
		if (blad == EMPTYQUEUEEXP)
		{
			cout << "kolejka pusta" << endl;
			system("pause");
		}

	}






	return 0;
}